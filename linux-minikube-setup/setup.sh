#!/bin/bash

# Check if dependency is already installed
command_exists() {
  command -v "$1" >/dev/null 2>&1
}

# Check if Docker is already installed
if ! command_exists docker; then
  # Install docker to run minikube
  echo "Installing Docker..."
  sudo apt-get update
  sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
  sudo apt-get update
  sudo apt-get install -y docker-ce docker-ce-cli containerd.io

  # Add current user to the docker group
  sudo usermod -aG docker "$USER"
  newgrp docker
else
  echo "Docker is already installed."
fi

# Install kubectl (if not already installed)
if ! command_exists kubectl; then
    echo "Installing kubectl..."
    sudo apt-get update
    sudo apt-get install -y apt-transport-https gnupg
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
    echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
    sudo apt-get update
    sudo apt-get install -y kubectl
else
    "Kubectl is already installed."
fi

# Check if Minikube is already installed
if ! command_exists minikube; then
  # Install Minikube
  echo "Installing Minikube..."
  curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
  # Permission to execute minicube
  chmod +x minikube
  sudo mv minikube /usr/local/bin/
  # Start Minikube cluster
  echo "Starting Minikube cluster..."
  minikube start --driver=docker
else
  echo "Minikube is already installed."
fi


echo "Minikube setup done."