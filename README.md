# Set up for web app deployment

## Instruction for Windows environment

### Prerequisites:
- Docker hub account to push built docker image
- Docker installed and running - [Installation guide](https://docs.docker.com/get-docker/) or install with choco:
```bash 
choco install docker-desktop
```
- Minikube installed and running - [Installation guide](https://minikube.sigs.k8s.io/docs/start/) or install with choco:
```bash 
choco install minikube
```
- Kubectl installed - [Installation guide](https://kubernetes.io/docs/tasks/tools/install-kubectl-windows/) or install with choco:
```bash 
choco install kubernetes-cli
```
- Jenkins installed and running - [Installation guide](https://www.jenkins.io/doc/book/installing/windows/) or install with choco:
```bash 
choco install jenkins
```
## Set up environment

### Connect Jenkins with Minikube

- Run commands to create service account for jenkins to connect to minikube cluster:
```bash
$ kubectl create namespace jenkins
$ kubectl create sa jenkins -n jenkins
$ kubectl create token jenkins -n jenkins --duration=8760h
```
- Copy the output of last command to later use it in Jenkins for connection
- Give permission to Jenkins service account to deploy resources
```bash
$ kubectl create rolebinding jenkins-sa-binding \
  --clusterrole=admin \
  --serviceaccount=jenkins:jenkins \
  --namespace=jenkins

$ kubectl create rolebinding jenkins-sa-binding-default \
  --clusterrole=admin \
  --serviceaccount=jenkins:jenkins \
  --namespace=default

```

- Log in to Jenkins Dashboard
- Set up connection to cloud:
    - On the left panel go to _Manage Jenkins_
    - In _System Configuration_ select _Clouds_
    - Choose Kubernetes and give a name to the connection, click Create
    - Insert _Kubernetes URL_, to retrieve it run command in powershell and copy url from line _server_:
    ```bash
    kubectl conifg view
    ```
    - Add new credentials as secret text, copy the value of jenkins token
    - Test the connection

### Set up new pipeline and connect to gitlab repository

- Set up Jenkins pipeline:
    - Go to Jenkins Dashboard 
    - On the left side click _New Item_
    - Name it and select _Pipeline_
    - In _Build Triggers_ check _Build when a change is pushed to GitLab_ in dropdown menu  check _Push Events_
    - Copy _Gitlab webhook URL_ to add to gitlab later
    - In _Pipeline_ section choose _Pipeline script from SCM_
    - For SCM choose Git
    - In _Repository URL_ paste gitlab url with all the files - https://gitlab.com/devopspr2/tet_test.git
    - Change _Branch Specifier_ from master to main and save pipeline

- Set up Gitlab webhook:
    - Install Gitlab Plugin in Jenkins (Go to Manage Jenkins -> Plugins -> Available Plugins -> Search Gitlab Plugin -> Install )
    - Go to Gitlab 
    - On the left side choose _Settings_ and _Webhooks_
    - Add new webhook and paste the _Gitlab webhook URL_ in _URL_ section 
    - As _Trigger_ check _Push events_
    - Enable the webhook

### Add credentials
- Dokcer credentials
    - In Docker hub go to your profile 
    - Create Access Token
    - Add credentials to Jenkins credentials as username and password
- Cluster kubeconfig:
    - Add to Jenkins credentials as file which you can find in file explorer as /.kube/config

### Run Jenkins pipeline

- Go to Jenkins dashboard 
- Click on the name of your pipeline
- Choose Build with Parameters
- In the DOCKER_HUB_REPO parameter add your github repository for image in format /yourusername/imagename (Repository should be created before running pipeline)
- Build the pipeline

### Access web application 

- Go to powershell terminal 
- Run ```minikube tunnel`` 
- Access the application in your browser
